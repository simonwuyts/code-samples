var IE = (!! window.ActiveXObject && +(/msie\s(\d+)/i.exec(navigator.userAgent)[1])) || NaN;
if (IE < 11) {
    document.documentElement.className += ' ie10';
}
if (IE < 10) {
    document.documentElement.className += ' ie9';
}

$(document).ready(function(){

  // Text input placeholder attribute polyfill

  $('input, textarea').placeholder();

  // Quick contact form triggers

  $('.js-quick-contact-trigger').click(function(){
    $(this).velocity('fadeOut', {duration: 200});
    $(this).next('.quick-contact-slideout').velocity('slideDown', {duration: 500});
    return false;
  });

  $('input[type=radio][name=callmail]').on('change', function(){
    if(this.value == 'call') {
      console.log('call');
      $(this).parents('.quick-contact').find('.call-wrapper').show();
      $(this).parents('.quick-contact').find('.mail-wrapper').hide();
    }
    if(this.value == 'mail') {
      console.log('mail');
      $(this).parents('.quick-contact').find('.mail-wrapper').show();
      $(this).parents('.quick-contact').find('.call-wrapper').hide();
    }
  });

  // Touch menu

  $('.js-touchmenu-trigger').click(function(){
    $('.touchmenu').addClass('open');
    $('.touchmenu-cover').addClass('open');
    $('body').addClass('lock-scroll');
    return false;
  });

  $('.touchmenu-close, .touchmenu-cover').click(function(){
    $('.touchmenu').removeClass('open');
    $('.touchmenu-cover').removeClass('open');
    $('body').removeClass('lock-scroll');
    return false;
  });

});
