(function() {

  function headerAnimation(el, options) {
    this.el = el;
    this.init();
  }

  headerAnimation.prototype.init = function() {
    this.trigger = document.querySelector('.logo');
    this.shapeEl = this.el.querySelector('.header-wrapper__backdrop');

    var s = Snap(this.shapeEl.querySelector('svg'));
    this.pathEl = s.select('path');
    this.paths = {
      start : 'M0,200 L0,200 c0,0 100,0 200,0 c0,0 100,0 200,0 L400,200 L0,200 Z',
      intermediate : 'M0,200 L0,150 c0,0 100,-75 200,-75 c0,0 100,0 200,75 L400,200 L0,200 Z',
      end : 'M0,200 L0,0 c0,0 100,0 200,0 c0,0 100,0 200,0 L400,200 L0,200 Z'
    };

    this.initEvents();
  };

  headerAnimation.prototype.initEvents = function() {
    this.trigger.addEventListener('click', this.fill.bind(this));
  };

  headerAnimation.prototype.fill = function() {
    var self = this;

    this.pathEl.stop().animate({ 'path' : this.paths.intermediate }, 350, mina.easeout, function() {
      self.pathEl.stop().animate({ 'path' : self.paths.end }, 800, mina.elastic);
    });

  };

  new headerAnimation(document.querySelector('.header-wrapper'));

})();

$(document).ready(function(){

});
