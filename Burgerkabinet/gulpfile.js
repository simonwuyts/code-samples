// Plugin imports
var gulp = require('gulp');
var rename = require('gulp-rename');
var fileinclude = require('gulp-file-include');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var newer = require('gulp-newer');
var imagemin = require('gulp-imagemin');
var clean = require('gulp-clean');
var connect = require('gulp-connect');

// HTML functions
gulp.task('html', function() {
	return gulp.src('templates/*.tpl.html')
		.pipe(fileinclude())
		.pipe(rename({
			extname: ''
		}))
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(gulp.dest('build'))
		.pipe(connect.reload());
});

// CSS functions
gulp.task('css', function() {
    return gulp.src('scss/*.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('build/css'))
        .pipe(connect.reload());
});

// Javascript functions
gulp.task('js', function() {
	return gulp.src('js/*.js')
		.pipe(concat('all.js'))
		.pipe(gulp.dest('build/js'))
		.pipe(connect.reload());
});

gulp.task('js-vendor', function() {
	return gulp.src('js/vendor/*.js')
		.pipe(gulp.dest('build/js/vendor'))
		.pipe(connect.reload());
});

// Image functions
gulp.task('images', function() {
	return gulp.src('img/**/*.+(png|jpg|jpeg|svg)')
		.pipe(newer('build/img'))
		.pipe(imagemin())
		.pipe(gulp.dest('build/img'))
		.pipe(connect.reload());
});

// Remove build files
gulp.task('clean', function() {
	return gulp.src('build', {read: false})
		.pipe(clean());
});

// Local server
gulp.task('connect', function() {
	connect.server({
		root: 'build',
		livereload: true
	});
});

// Watch files for changes
gulp.task('watchfiles', function() {
    gulp.watch(['templates/*.html', 'includes/*.html'], ['html']);
    gulp.watch('scss/*.scss', ['css']);
    gulp.watch('js/*.js', ['js']);
		gulp.watch('js/vendor/*.js', ['js-vendor']);
    gulp.watch('img/**/*.+(png|jpg|jpeg|svg)');
});

// Default tasks
gulp.task('default', ['connect', 'html', 'css', 'js', 'js-vendor', 'images', 'watchfiles']);
