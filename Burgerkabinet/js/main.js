FusionCharts.ready(function () {
    var conversionChart = new FusionCharts({
        type: 'scatter',
        renderAt: 'chartdiv',
        width: '100%',
        height: '400',
        dataFormat: 'json',
        dataSource:
            {
    "chart": {

        //Drawing quadrants on chart
        "drawQuadrant" : "1",
        //"quadrantLabelTL": "Weinig consumptie / Veel participatie",
        //"quadrantLabelTR": "Veel consumptie / Veel participatie",
        //"quadrantLabelBL": "Weinig consumptie / Weinig participatie",
        //"quadrantLabelBR": "Veel consumptie / Weinig participatie",
        //Setting x quadrant value to 54
        "quadrantXVal": "5",
        //Setting y quadrant value to 12000
        "quadrantYVal": "250",
        "quadrantLineAlpha" : "50",
        "quadrantLineThickness" : "2",
        "quadrantLabelPadding": "10",

        "palette": "1",
        //"caption": "JOUW POSITIE",
        "captionFontSize": "0",
        "captionFontColor": "#DB5413",
        "yaxisname": "Cultuur creëren",
        "xaxisname": "Cultuur beleven",
        "xaxisnamefontsize": "13",
        "yaxisnamefontsize": "13",
        "xaxismaxvalue": "10",
        "xaxisminvalue": "0",
        "bgcolor": "FFFFFF",
        "legendshadow": "0",
        "legendborderalpha": "0",
        "canvasborderthickness": "0",
        "canvasborderalpha": "30",
        "divlinealpha": "0",
        "showregressionline": "0",
        "yaxisminvalue": "-20",
        "numbersuffix": "%",
        "animation": "0",
        "showborder": "0",
        "baseFont" : "Helvetica",
        "showLegend": "0",
        //"captionpadding": "20",
        "showtooltip": "0",
        "showalternatehgridcolor": "0",
        "showalternatevgridcolor": "0",
        "showYAxisValues": "0",
        "baseFontSize": "13",
    },
    "categories": [
        {
            "verticallinethickness": "1",
            "verticallinealpha": "0",
            "category": [
                {
                    "label": "",
                    "x": "1",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "2",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "3",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "4",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "5",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "6",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "7",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "8",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "9",
                    "showverticalline": "1"
                },
                {
                    "label": "",
                    "x": "10",
                    "showverticalline": "0"
                }
            ]
        }
    ],
    "dataset": [
        {
            "seriesname": "Alle",
            "color": "#bbbbbb",
            "anchorbgcolor": "#bbbbbb",
            "plotborderthickness": "0",
            "showplotborder": "1",
            "anchorsides": "4",
            "anchorradius": "4",
            "data": [
                {
"id": "INVEQ324_1",
"x": "3.9",
"y": "13.8",
},
{
"id": "INVEQ324_2",
"x": "1.6",
"y": "236.3",
},
{
"id": "INVEQ324_3",
"x": "3.9",
"y": "39.9",
},
{
"id": "INVEQ324_4",
"x": "6.4",
"y": "363.4",
},
{
"id": "INVEQ324_5",
"x": "8.1",
"y": "444.8",
}
            ]
        }
        ,
        {
            "id": "DS2",
            "seriesname": "Jouw positie",
            "anchorbgcolor": "#DB5413",
            "color": "#DB5413",
            "showplotborder": "0",
            "anchorradius": "8",
            "data": [
                {
                    "id": "INVMF324_1",
                    "x": "1.4",
                    "y": "32.2",
                }
            ]
        }
    ]
}
    });
    conversionChart.render();
});

$(document).ready(function(){

  // Tabs
  $('.tab').click(function(){
    target = $(this).attr('data-target');
    $('.tab').removeClass('tab--active');
    $(this).addClass('tab--active');
    $('.tab-content').hide();
    $('.tab-content[data-content=' + target + ']').show();
  });

  // Info

  $('.info-icon').click(function(){
    $(this).toggleClass('info-icon--active');
    $(this).parent().find('.info-content').toggle();
  });

});
